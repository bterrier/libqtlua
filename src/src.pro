TEMPLATE = lib
TARGET = libqtlua

QT = core gui widgets

HEADERS += \
    internal/Enum \
    internal/EnumIterator \
    internal/ListIterator \
    internal/Member \
    internal/MetaCache \
    internal/Method \
    internal/Property \
    internal/QMetaObjectWrapper \
    internal/QMetaValue \
    internal/QObjectIterator \
    internal/QObjectWrapper \
    internal/qtluaenum.hh \
    internal/qtluaenum.hxx \
    internal/qtluaenumiterator.hh \
    internal/qtlualistiterator.hh \
    internal/qtluamember.hh \
    internal/qtluamember.hxx \
    internal/qtluametacache.hh \
    internal/qtluametacache.hxx \
    internal/qtluamethod.hh \
    internal/qtluamethod.hxx \
    internal/qtluapoolarray.hh \
    internal/qtluaproperty.hh \
    internal/qtluaproperty.hxx \
    internal/qtluaqmetaobjectwrapper.hh \
    internal/qtluaqmetavalue.hh \
    internal/qtluaqmetavalue.hxx \
    internal/qtluaqobjectiterator.hh \
    internal/qtluaqobjectwrapper.hh \
    internal/qtluaqobjectwrapper.hxx \
    internal/qtluatableiterator.hh \
    internal/qtluatabletreekeys.hh \
    internal/qtluatabletreekeys.hxx \
    internal/TableIterator \
    internal/TableTreeKeys \
    QtLua/ArrayProxy \
    QtLua/Console \
    QtLua/DispatchProxy \
    QtLua/Function \
    QtLua/ItemViewDialog \
    QtLua/Iterator \
    QtLua/LuaModel \
    QtLua/MetaType \
    QtLua/Plugin \
    QtLua/QHashProxy \
    QtLua/QLinkedListProxy \
    QtLua/QListProxy \
    QtLua/qtluaarrayproxy.hh \
    QtLua/qtluaarrayproxy.hxx \
    QtLua/qtluaconsole.hh \
    QtLua/qtluaconsole.hxx \
    QtLua/qtluadispatchproxy.hh \
    QtLua/qtluadispatchproxy.hxx \
    QtLua/qtluafunction.hh \
    QtLua/qtluafunction.hxx \
    QtLua/qtluaitemviewdialog.hh \
    QtLua/qtluaitemviewdialog.hxx \
    QtLua/qtluaiterator.hh \
    QtLua/qtluaiterator.hxx \
    QtLua/qtlualuamodel.hh \
    QtLua/qtlualuamodel.hxx \
    QtLua/qtluametatype.hh \
    QtLua/qtluametatype.hxx \
    QtLua/qtluaplugin.hh \
    QtLua/qtluaplugin.hxx \
    QtLua/qtluaqhashproxy.hh \
    QtLua/qtluaqhashproxy.hxx \
    QtLua/qtluaqlinkedlistproxy.hh \
    QtLua/qtluaqlinkedlistproxy.hxx \
    QtLua/qtluaqlistproxy.hh \
    QtLua/qtluaqlistproxy.hxx \
    QtLua/qtluaqvectorproxy.hh \
    QtLua/qtluaqvectorproxy.hxx \
    QtLua/qtluaref.hh \
    QtLua/qtluastate.hh \
    QtLua/qtluastate.hxx \
    QtLua/qtluastring.hh \
    QtLua/qtluastring.hxx \
    QtLua/qtluatablegridmodel.hh \
    QtLua/qtluatablegridmodel.hxx \
    QtLua/qtluatabletreemodel.hh \
    QtLua/qtluatabletreemodel.hxx \
    QtLua/qtluauserdata.hh \
    QtLua/qtluauserdata.hxx \
    QtLua/qtluauseritem.hh \
    QtLua/qtluauseritem.hxx \
    QtLua/qtluauseritemmodel.hh \
    QtLua/qtluauseritemmodel.hxx \
    QtLua/qtluauseritemselectionmodel.hh \
    QtLua/qtluauseritemselectionmodel.hxx \
    QtLua/qtluauserlistitem.hh \
    QtLua/qtluauserlistitem.hxx \
    QtLua/qtluauserobject.hh \
    QtLua/qtluauserobject.hxx \
    QtLua/qtluavalue.hh \
    QtLua/qtluavalue.hxx \
    QtLua/qtluavaluebase.hh \
    QtLua/qtluavaluebase.hxx \
    QtLua/qtluavalueref.hh \
    QtLua/qtluavalueref.hxx \
    QtLua/QVectorProxy \
    QtLua/Ref \
    QtLua/State \
    QtLua/String \
    QtLua/TableGridModel \
    QtLua/TableTreeModel \
    QtLua/UserData \
    QtLua/UserItem \
    QtLua/UserItemModel \
    QtLua/UserItemSelectionModel \
    QtLua/UserListItem \
    QtLua/UserObject \
    QtLua/Value \
    QtLua/ValueBase \
    QtLua/ValueRef \
    qtluaqtlib.hh

DISTFILES += \
    internal/Makefile.am \
    QtLua/Makefile.am

SOURCES += \
    qtluaconsole.cc \
    qtluadispatchproxy.cc \
    qtluaenum.cc \
    qtluaenumiterator.cc \
    qtluafunction.cc \
    qtluaitemviewdialog.cc \
    qtlualistiterator.cc \
    qtlualuamodel.cc \
    qtluamember.cc \
    qtluametacache.cc \
    qtluamethod.cc \
    qtluaplugin.cc \
    qtluaproperty.cc \
    qtluaqmetaobjecttable.cc \
    qtluaqmetaobjectwrapper.cc \
    qtluaqmetavalue.cc \
    qtluaqobjectiterator.cc \
    qtluaqobjectwrapper.cc \
    qtluaqtlib.cc \
    qtluastate.cc \
    qtluatablegridmodel.cc \
    qtluatableiterator.cc \
    qtluatabletreekeys.cc \
    qtluatabletreemodel.cc \
    qtluauserdata.cc \
    qtluauseritem.cc \
    qtluauseritemmodel.cc \
    qtluauseritemselectionmodel.cc \
    qtluauserlistitem.cc \
    qtluavalue.cc \
    qtluavaluebase.cc \
    qtluavalueref.cc

//QMAKE_SUBSTITUTES += $${PWD}/../config.hh.in

!isEmpty( LUA_LIB_PATH ) {
LIBS += -L$${LUA_LIB_PATH}
}
!isEmpty( LUA_LIB_NAME ) {
LIBS += -l$${LUA_LIB_NAME}
}
!isEmpty( LUA_INCLUDE_PATH ) {
INCLUDEPATH += $${LUA_INCLUDE_PATH}
}
